import React, { Fragment } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
// Admin Routes
import Admin from './routes/admin';
import AdminVolunteers from './routes/admin/volunteers';
// App Routes
import App from './routes/app';
import AppVolunteers from './routes/app/volunteers';
import withTheme from './withTheme';

function Router() {
  return (
    <BrowserRouter>
      <Fragment>
        {/* App */}
        <Route exact path="/" component={App} />
        <Route exact path="/volunteers" component={AppVolunteers} />
        {/* Admin */}
        <Route exact path="/admin" component={Admin} />
        <Route exact path="/admin/volunteers" component={AdminVolunteers} />
      </Fragment>
    </BrowserRouter>
  );
}

export default withTheme(Router);
