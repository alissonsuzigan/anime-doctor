import firebase from 'firebase';

// Initialize Firebase
const config = {
  apiKey: 'AIzaSyBQRBzsgpMrV0PAdW5USGyvRB8P-xJzx9w',
  authDomain: 'anime-doctor.firebaseapp.com',
  databaseURL: 'https://anime-doctor.firebaseio.com',
  projectId: 'anime-doctor',
  storageBucket: 'anime-doctor.appspot.com',
  messagingSenderId: '303068556410',
};

export const firebaseInit = firebase.initializeApp(config);
export const firebaseData = firebase.database();

// Get items from Firebase
export const getDataList = (collection: string, callback: Function, size = 10): firebase.database.Query => {
  const query = firebaseData.ref(collection).limitToLast(size);
  query.on('value', (dataSnapshot) => {
    const items: any[] = [];
    dataSnapshot!.forEach((childSnapshot) => {
      const item = childSnapshot.val();
      item.key = childSnapshot.key;
      items.push(item);
    });
    callback(items);
  });

  return query;
};
