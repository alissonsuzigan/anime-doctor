import React from 'react';
import Layout from '../../components/layout';

function App() {
  return (
    <Layout>
      <h1>Home page App</h1>
    </Layout>
  );
}

export default App;
