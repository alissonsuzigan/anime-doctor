import React from 'react';
import ReactDOM from 'react-dom';
import Volunteers from '../volunteers';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Volunteers />, div);
  ReactDOM.unmountComponentAtNode(div);
});
