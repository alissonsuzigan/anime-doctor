import React from 'react';
import Layout from '../../components/layout';

function Admin() {
  return (
    <Layout admin>
      <h1>Home page Admin</h1>
    </Layout>
  );
}

export default Admin;
