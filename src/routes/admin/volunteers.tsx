import React, { useState, ChangeEvent } from 'react';
import {
  Button, Paper, TextField, Theme, Typography, WithStyles,
  createStyles, withStyles,
} from '@material-ui/core';
import Layout from '../../components/layout';

const INITAL_STATE = { name: '', email: '', role: '', phone: '', birthday: '' };

function Volunteers({ classes }: WithStyles) {
  const [state, setState] = useState(INITAL_STATE);

  const onChange = (field: string) => (event: ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, [field]: event.target.value });
  };

  return (
    <Layout admin>
      <Paper className={classes.paper}>
        <Typography variant="h1">Dados do voluntário</Typography>

        <form className={classes.form} noValidate autoComplete="off">
          <TextField
            id="name"
            label="Nome"
            className={classes.textField}
            value={state.name}
            onChange={onChange('name')}
            margin="normal"
            required
          />
          <TextField
            id="email"
            label="e-mail"
            className={classes.textField}
            value={state.email}
            onChange={onChange('email')}
            margin="normal"
            required
          />
          <TextField
            id="role"
            label="Função"
            className={classes.textField}
            value={state.role}
            onChange={onChange('role')}
            margin="normal"
            required
          />
          <TextField
            id="phone"
            label="Telefone"
            className={classes.textField}
            value={state.phone}
            onChange={onChange('phone')}
            margin="normal"
            required

          />
          <TextField
            id="birthday"
            label="Nascimento"
            className={classes.textField}
            value={state.birthday}
            onChange={onChange('birthday')}
            margin="normal"
            required
          />
          <Button color="primary" variant="contained" size="large">Salvar</Button>
        </form>
      </Paper>
    </Layout>
  );
}


const styles = (theme: Theme) => createStyles({
  paper: {
    maxWidth: 600,
    margin: '1rem auto',
    padding: theme.spacing.unit * 3,
    [theme.breakpoints.down('xs')]: {
      margin: 0,
      padding: theme.spacing.unit * 2,
    },
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    '&:last-of-type': {
      marginBottom: theme.spacing.unit * 4,
    },
  },
});

export default withStyles(styles)(Volunteers);
