import React from 'react';
import { render, cleanup } from 'react-testing-library';
// import { render, fireEvent, cleanup, waitForElement } from 'react-testing-library';
import Volunteers from '../volunteers';

afterEach(cleanup);

describe('Volunteers form component', () => {
  it('displays the header title', () => {
    const { getByText } = render(<Volunteers />);
    expect(getByText('Dados do voluntário')).toBeDefined();
  });

  it('displays the form labels, fields and button', () => {
    const { getByLabelText, getByText } = render(<Volunteers />);
    expect(getByLabelText(/Nome/)).toBeDefined();
    expect(getByLabelText(/e-mail/)).toBeDefined();
    expect(getByLabelText(/Função/)).toBeDefined();
    expect(getByLabelText(/Telefone/)).toBeDefined();
    expect(getByLabelText(/Nascimento/)).toBeDefined();
    expect(getByText(/Salvar/)).toBeDefined();
  });
});
