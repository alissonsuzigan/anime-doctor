import React from 'react';
import { AppBar, Toolbar, Typography } from '@material-ui/core';

function Header({ admin = false }: { admin?: boolean }) {
  const styles = {
    adminBar: { background: '#222' },
    title: { fontSize: 21 },
  };

  return (
    <AppBar position="static" style={admin ? styles.adminBar : undefined}>
      <Toolbar variant={admin ? 'dense' : undefined}>
        <Typography color="inherit" variant="body2" style={styles.title}>
          {admin && <span>Admin: </span>}
          Anime Doctor
        </Typography>
      </Toolbar>
    </AppBar>
  );
}

export default Header;
