import React from 'react';
import { render, cleanup } from 'react-testing-library';
import Header from './header';

afterEach(cleanup);

describe('Header component', () => {
  describe('on App rendering', () => {
    it('displays the header title', () => {
      const { getByText } = render(<Header />);
      expect(getByText('Anime Doctor')).toBeDefined();
    });
  });

  describe('on Admin rendering', () => {
    it('displays the header title', () => {
      const { getByText } = render(<Header admin />);
      expect(getByText('Admin:')).toBeDefined();
      expect(getByText('Anime Doctor')).toBeDefined();
    });
  });
});
