import React from 'react';
import ReactDOM from 'react-dom';
import Layout from './layout';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const component = <Layout><div>Test</div></Layout>;
  ReactDOM.render(component, div);
  ReactDOM.unmountComponentAtNode(div);
});
