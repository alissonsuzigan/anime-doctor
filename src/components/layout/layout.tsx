import React, { Fragment, ReactNode } from 'react';
import { createStyles, withStyles } from '@material-ui/core';
import Header from '../header';

function Layout(
  { admin = false, children, classes, ...props }:
  { admin?: boolean; children: ReactNode; classes: Record<string, string> },
) {
  return (
    <Fragment {...props}>
      <Header admin={admin} />
      <main className={classes.main}>{children}</main>
    </Fragment>
  );
}

const styles = createStyles({
  main: {
    overflow: 'auto',
  },
});

export default withStyles(styles)(Layout);
