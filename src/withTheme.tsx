import React, { ComponentType } from 'react';
import { MuiThemeProvider, createMuiTheme, CssBaseline } from '@material-ui/core';
// import indigo from '@material-ui/core/colors/indigo';
// import green from '@material-ui/core/colors/green';

// A theme with custom primary and secondary color.
const theme = createMuiTheme({
  // palette: {
  //   primary: {
  //     light: indigo[300],
  //     main: indigo[500],
  //     dark: indigo[700],
  //   },
  //   secondary: {
  //     light: green[300],
  //     main: green[500],
  //     dark: green[700],
  //   },
  // },
  overrides: {
    MuiTypography: {
      h1: { fontSize: '2.8rem' },
      h2: { fontSize: '2.5rem' },
      h3: { fontSize: '2.2rem' },
      h4: { fontSize: '1.9rem' },
      h5: { fontSize: '1.6rem' },
      h6: { fontSize: '1.3rem' },
    },
  },
  typography: {
    useNextVariants: true,
  },
});

function withTheme(Component: ComponentType) {
  function WithTheme(props: object) {
    // MuiThemeProvider makes the theme available down the React tree thanks to React context.
    return (
      <MuiThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <Component {...props} />
      </MuiThemeProvider>
    );
  }
  return WithTheme;
}

export default withTheme;
