module.exports = {
  env: {
    browser: true,
    jest: true
  },
  extends: ['airbnb', 'plugin:@typescript-eslint/recommended'],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  parserOptions: {
    project: './tsconfig.json',
  },
  settings: {
    'import/resolver': {
      node: { extensions: [".ts", ".tsx"] }
    }
  },
  rules: {
    'max-len': [2, { code: 120 }],
    'object-curly-newline': [0, { 'multiline': true, 'minProperties': 6 }],
    'react/jsx-filename-extension': [2, { extensions: ['.ts', '.tsx'] }],
    '@typescript-eslint/indent': [2, 2],
    '@typescript-eslint/interface-name-prefix': [2, 'always'],
    '@typescript-eslint/explicit-function-return-type': [0],
  },
};
